/**
 * En este paquete se encuentra la clase MiArrayList, en ella se hallan todos los métodos implementados en la practica anterior mas 2 nuevos métodos necesarios en esta practica que son
 * el sort ( ordenar los elementos del array segun el criterio seleccionado) y el método comparar ( metodo privado que te compara un elemento con otro ). El algoritmo de ordenacion que 
 * hemos implementado en este caso es el burbuja.
 */
package listas;
