/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import excepciones.ExcepcionIndiceFueraDelIntervaloValido;
import java.util.Comparator;

/**
 *
 * @author Alejadro
 */
public class MiArrayList<E extends Comparable> {

    int tamMax = 10;
    int tam = 0;
    Object[] array = new Object[tamMax];
    // lo que significa que el array tiene 10 posiciones
    // de la posicion array[0] a la posicion array[9]

    public void add(E e) { // complejidad constante si no hay que duplicar el array
        //complejidad lineal si hay que duplicarlo
//        array[tam++] = e;
        array[tam] = e;
        if (tam < tamMax - 1) {
            tam = tam + 1;
        } else {
            tamMax = tamMax * 3;
            tam = tam + 1;
            Object[] nuevoarray = new Object[tamMax];
            // Algoritmo para copiar el array a nuevoarray.
            for (int i = 0; i < array.length; i++) {
                nuevoarray[i] = array[i];
            }
            array = nuevoarray;

        }
    }

    // método no pedido por Mariano
    // te devuelve la posición del array en la que se encuentra el elemento buscado
    public int buscarElemento(E e) { //complejidad lineal
        boolean elementoEncontrado = false;
        int indice = -1;
        for (int i = 0; i < size() && !elementoEncontrado; i++) {
            elementoEncontrado = e.equals(array[i]);
            if (elementoEncontrado) {
                indice = i;
            }
        }

        return indice;
    }

    // método que te borra el elemento buscado 1 vez
    public void remove(E e) { // complejidad lineal + lineal=  complejidad lineal
        int posicionDelElementoABorrar = buscarElemento(e);
        boolean elementoEncontrado = posicionDelElementoABorrar != -1;
        if (elementoEncontrado) {
            for (int i = posicionDelElementoABorrar; i < size(); i++) {
                array[i] = array[i + 1];
            }
            this.tam--;
        } else {
            System.out.println("El elemento no está en el array");
        }
    }

    public E get(int i) { //complejidad constante
        // método para recuperar el elemento del array,
        // indicando la posición que ocupa en este
        if (i < 0) {
            return null;
        } else {
            return (E) array[i];
        }
    }

    public boolean isEmpty() { // complejidad constante
        return size() == 0;
    }

    // Método hecho sin tdd
    public void mostrar() {
        for (Object e : array) {
            System.out.println((E) e);

        }
    }

    public boolean contains(E e) { //complejidad lineal
        boolean elementoContenido = false;
        for (int i = 0; i < tam; i++) {
            if (e.equals(array[i])) {
                elementoContenido = true;
            }
        }
        return elementoContenido;
    }

    // método que te devuelve el número de elementos no nulos del array
    public int size() { //complejidad constante
        return tam;
    }

    public void sort(Comparator comparador) {

        boolean ordenado = false;
        int cuentaIntercambios = 0;
        //Usamos un bucle anidado, saldra cuando este ordenado el array
        while (!ordenado) {
            for (int i = 0; i < this.size() - 1; i++) {
                if (comparar((E) array[i], (E) array[i + 1], comparador) > 0) {
                    //Intercambiamos valores
                    Object aux = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = aux;
                    //indicamos que hay un cambio
                    cuentaIntercambios++;
                }
            }
            //Si no hay intercambios, es que esta ordenado.
            if (cuentaIntercambios == 0) {
                ordenado = true;
            }
            //Inicializamos la variable de nuevo para que empiece a contar de nuevo
            cuentaIntercambios = 0;
        }

    }
    
    private int comparar(E elemento1, E elemento2, Comparator comparador)
    {
        if (comparador == null) return elemento1.compareTo(elemento2);
        else return comparador.compare(elemento1, elemento2);
    }

}
